//
//  FKController.h
//  Magyaritas Alap
//
//  Created by Ferenc Kiss on 2013.07.05..
//
//

#import <Cocoa/Cocoa.h>
//#import <Foundation/Foundation.h>

/*!
 *  \brief     Ez a vezérlést biztosító fájl.
 *  \details   Ezen keresztül egyszerűen elkészíthetjük a telepítő alkalmazást.
 *  \author    Kiss Ferenc
 *  \version   1.0
 *  \date      2013
 *  \pre       Objektum létrehozásánál az initWithDefaultPath: funkciót érdemes használni.
 *  \pre       Objektum létrehozásánál a adjunk meg mindenképpen egy backup mappát.
 *  \warning   Mindenképpen legyen beállítva a defaultPath a használathoz!
 *  \copyright GNU Public License.
 */
@interface FKController : NSObject {
	BOOL uninstallerEnabled;						// bekapcsoltuk-e az uninstallert
	BOOL uninstallingOrReinstalling;								// uninstall-t akarunk-e vegezni, mert akkor nem kell leallitani a program mukodeset
	
	
	NSString *defaultPath;						// ezt a felhasznalo mondja meg, hogy hol van
	NSString *backupFolderFullPath;				// melyik mappaba keszitsuk uninstaller fileokat
	
	
	NSMutableArray *filesToPatch;				// ebben taroljuk, hogy milyen fajlokat kell patchelni
	NSMutableArray *filesToCopy;				// ebben taroljuk, hogy milyen fajlokat kell csak atmasolni
	NSMutableArray *filesToReplace;				// ebben pedig azt taroljuk, hogy milyen fajlokat kell kicserelni
	NSMutableArray *filesToDeleteAfterSuccesFullInstall;	// ebben pedig azokat taroljuk, amiket telepites utan torolni kell
	
	NSMutableArray *uninstallerPatchFiles;
	NSMutableArray *uninstallerDeleteFiles;
	NSMutableArray *uninstallerReplaceFiles;
	
	NSFileManager *mainManager;
	
	NSString *tempDir;
	
	AuthorizationRef authRef;
}

/*!
 *	Ezzel a tulajdonsággal kapcsolhatjuk ki, hogy ha nem akarunk készíteni mentést.
 *	\note	getter = - (BOOL) isUninstallerEnabled;
 *	\note	setter = setUninstallerEnabled: (BOOL) enabled;
 */
@property (getter = isUninstallerEnabled) BOOL uninstallerEnabled;

/*!
 *	Ezzel a tulajdonsággal kapcsolhatjuk be, hogy uninstallálni szeretnénk, esetleg még egyszer szeretnénk telepíteni, és ne álljon le a telepítő fájlok hozzáadásakor, ha hiba van.
 *	\note	getter = - (BOOL) isUninstalling;
 *	\note	setter = setUninstalling: (BOOL) enabled;
 */
@property (getter = isUninstallingOrReinstalling) BOOL uninstallingOrReinstalling;

/*!
 *	Ezzel a tulajdonsággal adhatjuk meg a alap útvonalat később.
 *	\note	getter = - (NSString *) defaultPath;
 *	\note	setter = setDefaultPath: (NSString *) aDefaultPath;
 */
@property (readwrite, copy) NSString *defaultPath;

- (id) initWithDefaultPath: (NSString *) aDefaultPath relativeBackupPath: (NSString *) aRelativeBackupPath enableUninstaller: (BOOL) aEnableUninstaller;
- (BOOL) isValidDefaultPath;
- (BOOL) addNewPatchFile: (NSString *) patchFile originalFileRelativePath: (NSString *) originalRelativeFilePath reversePathFile: (NSString *) reversePathFile originalFileSize: (NSNumber *) originalFileSize patchedFileSize: (NSNumber *) patchedFileSize;
- (BOOL) addNewCopyFile: (NSString *) copyFilePath newRelativePath: (NSString *) newRelativePath;
- (BOOL) addNewReplaceFile: (NSString *) newFilePath originalRelativePath: (NSString *) originalRelativePath originalFileSize: (NSNumber *) originalFileSize;
- (BOOL) install;
- (BOOL) uninstall;

//#pragma mark - other functions
//- (BOOL) isFilePath: (NSString *) path equalToMD5Hash: (NSString *) hash;

@end
