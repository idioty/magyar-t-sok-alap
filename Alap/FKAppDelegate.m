//
//  FKAppDelegate.m
//  Magyaritas Alap
//
//  Created by Ferenc Kiss on 2013.07.05..
//
//

#import "FKAppDelegate.h"
#import "FKController.h"

/*! \example FKController.m
 *	1.	Elsőként másoljuk be, és adjuk a futtatható fájlunkhoz a módosításhoz szükséges fájlokat (pl.: patch és kicserélendő fájlok)
 *	2.	Kérdezzük meg a felhasználót a telepíteni kívánt mappa (vagy alkalmazás) helyét.
 *	3.	Ha minden adat megvan, és rányom a telepítésre, akkor hozzuk létre egy példányt az FKController osztályból.
 *		-	Itt adjuk meg a felhasználó által megadott mappa helyét,
 *		-	És azt, hogy kell-e létrehozni backup fájlokat.
 *	4. Adjuk hozzá az FKController példányunkhoz a módosító fájlokat:
 *		-	addNewPatchFile:originalFileRelativePath:reversePathFile: funkcióval hozzáadhatunk egy patchelni kívánt fájlt. Itt meg kell adni a patch fájlt (amit bemásoltunk az első lépésnél), a patchelni kívánt fájl relatív elérési útvonalát, és az uninstaller visszaállító patch fájlt (ha uninstall be van kapcsolva)
 *		-	addNewCopyFile:newRelativePath: ezzel a funkcióval hozzáadhatunk egy oylan fájlt, amit csak bemásolni kell a helyére. Meg kell adni a bemásolandó fájlt, és a relatív útvonalát, hogy hova akarjuk majd másolni.
 *		-	addNewReplaceFile:originalRelativePath: itt megadhatunk egy fájlt, amit ki szeretnénk cserélni egy másikra. Elsőként meg kell adni, hogy mit akarunk másolni, és másodikként a relatív útvonalat, hogy hova akarjuk másolni.
 *	5.	Kérdezzük meg, hogy készítsünk-e backup fájlt. Persze itt megadhatjuk, hogy mennyi helyet foglal. És ezt ki tudjuk kapcsolni a példány uninstallerEnabled funkcióval.
 *	6.	Elindíthatjuk a telepítést az install funkcióval.
 *
 *	Lehetőségünk van lekérdezni, hogy léteznek-e már a visszaállítás fájlok, azért, hogy felajánljuk az első lépésként, hogy uninstallálást akarja választni. Persze ehhez az install után el kell menteni az eredeti mappa helyét, hogy le tudjuk ellenőrizni, hogy létre vannak-e már hozva.
 *		És midnenképpen a lekérdezés előtt szintén hozzá kell adni a 4. pont szerint a dolgokat, mert abból számítja ki a létező fájlokat.
 */

@implementation FKAppDelegate

- (void) applicationDidFinishLaunching: (NSNotification *) notification {
	// 2. lépés
	NSString *defaultPath = @"/Users/idioty/Desktop/probapatch";
	
	// 3. lépés
	controller = [[FKController alloc] initWithDefaultPath: defaultPath relativeBackupPath: @"backups" enableUninstaller: YES];
	if (!controller) {
		NSLog(@"nem sikerult a controller letrehozasa");
		return;
	}
	
	// 4. lépés
	if (![controller addNewPatchFile: [[NSBundle mainBundle] pathForResource: @"DataMAC_extra" ofType: @"dat"] originalFileRelativePath: @"DataMAC_extra.forge" reversePathFile: @""])
		NSLog(@"nem sikeralt hozzaadni a patchfilet he");
	if (![controller addNewReplaceFile: [[NSBundle mainBundle] pathForResource: @"warning_disclaimer" ofType: @"bik"] originalRelativePath: @"warning_disclaimer.bik"])
		NSLog(@"nem sikeralt hozzaadni a kicserelendo fajlt");
	
	// 5. lépés
	// most azt mondjuk, hogy nem kerdezzuk meg, az elejen belallitottuk, es kesz...
	
	// 6. lépés
	if (![controller install]) {
		[controller uninstall]; // ha nem sikerult az install, akkor megprobaljuk visszaallitani a fajlokat az eredeti szerint
		NSLog(@"nem sikerult az install");
	}
}

@end
