//
//  FKAppDelegate.h
//  Magyaritas Alap
//
//  Created by Ferenc Kiss on 2013.07.05..
//
//

#import <Cocoa/Cocoa.h>

/*!
 *  \brief     Példa leírást készítünk.
 *  \details   Ezen keresztül egyszerűen elkészíthetjük a telepítést
 *  \author    Kiss Ferenc
 *  \version   1.0
 *  \date      2013
 *  \copyright GNU Public License.
 */
@class FKController;

@interface FKAppDelegate : NSObject <NSApplicationDelegate> {
	FKController *controller;
}

@end
