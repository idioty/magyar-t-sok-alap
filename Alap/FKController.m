//
//  FKController.m
//  Magyaritas Alap
//
//  Created by Ferenc Kiss on 2013.07.05..
//
//


#import <CommonCrypto/CommonDigest.h>

/*
@interface NSString (MD5)

- (NSString *)MD5String;

@end

@implementation NSString (MD5)

- (NSString *)MD5String {
	const char *cstr = [self UTF8String];
	unsigned char result[16];
	CC_MD5(cstr, (CC_LONG)strlen(cstr), result);
	
	return [NSString stringWithFormat:
			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0], result[1], result[2], result[3],
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			];
}

@end
 */

/*
@interface NSData (MD5)

- (NSString *) md5String;

@end

@implementation NSData (MD5)

- (NSString *) md5String {
    unsigned char md5[CC_MD5_DIGEST_LENGTH];
    CC_MD5([self bytes], (CC_LONG)[self length], md5);
    return [NSString stringWithFormat: @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            md5[0], md5[1],
            md5[2], md5[3],
            md5[4], md5[5],
            md5[6], md5[7],
            md5[8], md5[9],
            md5[10], md5[11],
            md5[12], md5[13],
            md5[14], md5[15]
            ];
}

- (BOOL) isEqualToMD5Hash: (NSString *) hash {
	return [[[self md5String] lowercaseString] isEqualToString: [hash lowercaseString]];
}

@end
*/

#define fPatchFile					@"patchFile"
#define fReversePatchFile			@"reversePatchFile"
#define fOriginalFilePath			@"originalFilePath"

#define fCopyFilePath				@"copyFilePath"
#define fNewFilePath				@"newFilePath"

#define fOriginalFileSize			@"originalFileSize"
#define	fPatchedFileSize			@"patchedFileSize"


#import "FKController.h"
#include "XDeltaAdapter.h"

@interface FKController (Own)

@end


@implementation FKController

@synthesize defaultPath;
@synthesize uninstallerEnabled, uninstallingOrReinstalling;
#pragma mark - init, dealloc
- (id) init {
	self = [super init];
	
	if (self) {
		filesToPatch = [NSMutableArray new];
		filesToCopy = [NSMutableArray new];
		filesToReplace = [NSMutableArray new];
		filesToDeleteAfterSuccesFullInstall = [NSMutableArray new];
		
		uninstallerPatchFiles = [NSMutableArray new];
		uninstallerDeleteFiles = [NSMutableArray new];
		uninstallerReplaceFiles = [NSMutableArray new];
		
		defaultPath = @"";
		backupFolderFullPath = @"";
		
		uninstallerEnabled = NO;
		self.uninstallingOrReinstalling = NO;
		
		mainManager = [NSFileManager defaultManager];
	}
	
	return  self;
}

/*!
 *			Ezzel egy húzásra beállíthatjuk az alap könyvtárat (vagy alkalamzást, ami szintén könyvtár...).
 * \param	aDefaultPath a teljes elérése a könyvtárnak, ahol dolgozni akarunk.
 * \param	aRelativeBackupPath ide mentjük ideiglenesen a fájlokat
 * \param	aEnableUninstaller ezzel a paraméterrel adhatjuk meg már a kezdetekkor, hogy szerenénk-e uninstallert készíteni.
 * \pre		defaultPath változó nem lehet üres string.
 * \pre		az aRelativeBackupPath nem lehet nil, üres string lehet. Meg kell adni mindenképpen a mentés helyét.
 * \return	Ha nem létezik a path, akkor nil értékkel térünk vissza.
 */
- (id) initWithDefaultPath: (NSString *) aDefaultPath relativeBackupPath: (NSString *) aRelativeBackupPath enableUninstaller: (BOOL) aEnableUninstaller {
	self = [self init];
	
	if (self) {
		defaultPath = [aDefaultPath copy];
		
		BOOL isDir;
		if (![mainManager fileExistsAtPath: defaultPath isDirectory: &isDir]) {
			[self release];
			return nil;
		}
		else {
			if (!isDir) {
				[self release];
				return nil;
			}
			
			backupFolderFullPath = [defaultPath stringByAppendingPathComponent: aRelativeBackupPath];
		}
		
		uninstallerEnabled = aEnableUninstaller;
	}
	
	return self;
}

- (void) dealloc {
	[defaultPath release];
	[backupFolderFullPath release];
	
	[filesToPatch release];
	[filesToCopy release];
	[filesToReplace release];
	[filesToDeleteAfterSuccesFullInstall release];
	
	[uninstallerPatchFiles release];
	[uninstallerDeleteFiles release];
	[uninstallerReplaceFiles release];
	
	[super dealloc];
}

/*!
 *			Ezzel kapcsolhatjuk be az uninstall létrehozását.
 * \param	aRelativePath a defaultPath-hez képest hol kellene lennie a mappának. A teljes mappa nev is kell!
 * \pre		defaultPath mindenképpen egy létező mappára mutasson.
 * \return	Igaz, ha elérhető az uninstallnak beállított path.
 */
- (BOOL) createBackupFolderWithRelativePath; {
	// ha nincs beallitva path, akkor kilepunk
	if (![self isValidDefaultPath])
		return NO;
	
	BOOL *isDir = NULL;
	if ([mainManager fileExistsAtPath: backupFolderFullPath isDirectory: isDir]) {
		return isDir;
	}
	else {
		NSString *lastPatch = [backupFolderFullPath stringByDeletingLastPathComponent];
		if (![mainManager isWritableFileAtPath: lastPatch]) {
			NSLog(@"nem irhato a backupnak a mappaja");
			return NO;
		}
		else if (![mainManager createDirectoryAtPath: backupFolderFullPath withIntermediateDirectories: NO attributes: nil error: nil]) {
			NSLog(@"[%s] Nem sikerült a backup mappa létrehozása: %@!", __FUNCTION__, backupFolderFullPath);
			backupFolderFullPath = @"";
			return NO;
		}
	}
	
	return YES;
}

/*!
 *			Ezzel ellenőrizhetjük le, hogy érvényes mappáról van-e szó.
 * \return	Igaz, ha érvényes elérési útról van szó
 */
- (BOOL) isValidDefaultPath {
	// ha nincs beallitva path, akkor kilepunk
	if (!defaultPath || [defaultPath isEqualToString: @""])
		return NO;
	
	BOOL isDir;
	if ([mainManager fileExistsAtPath: defaultPath isDirectory: &isDir])
		return isDir;
	else
		return NO;
}

#pragma mark - setup installer files
/*!
 *			Hozzáadjuk a patchet, és hogy mit kell patchelni.
 * \param	patchFile a patch helye.
 * \param	originalRelativeFilePath eredeti fájl, amit patchelni szeretnénk.
 * \param	reversePathFile visszaállítás patch fájl.
 ÷ \param	originalFileSize eredeti fájl mérete bájtban.
 ÷ \param	patchedFileSize patchelt fájl mérete bájtban.
 * \pre		Érvényes fájl elérési útnak kell lennie az aPathFile-nak
 * \pre		Érvényes elérési útnak kell lennie a defaultPath változónak is.
 * \pre		Érvényes elérési útnak kell lennie az originalRelativeFilaPath változónak, ha defaultPath után tesszük.
 * \pre		Érvényes elérési útnak kell lennie az reversePathFile változónak, ha nem nil vagy üres string az értéke.
 * \pre		originalFileSize paraméter nem lehet nil, vagy 0 értékű
 * \note	reversePathFile lehet üres string vagy nil érték is, ha nem akarunk visszaállítást csinálni.
 * \return	Ha sikerült hozzáadni a listához.
 */
- (BOOL) addNewPatchFile: (NSString *) patchFile originalFileRelativePath: (NSString *) originalRelativeFilePath reversePathFile: (NSString *) reversePathFile originalFileSize: (NSNumber *) originalFileSize patchedFileSize: (NSNumber *) patchedFileSize {
	// ha nincs beallitva path, akkor kilepunk
	if (![self isValidDefaultPath]) {
		NSLog(@"[%s] Nem valós a defaultPath!", __FUNCTION__);
		return NO;
	}
	
	if (!originalFileSize || [originalFileSize unsignedLongLongValue] == 0) {
		NSLog(@"[%s] Nem érvényes az originalFileSize paraméter!", __FUNCTION__);
		if (!self.uninstallingOrReinstalling)
			return NO;
	}
	
	if (![mainManager fileExistsAtPath: patchFile]) {
		NSLog(@"[%s] Nem létezik az aPatchFile", __FUNCTION__);
		if (!self.uninstallingOrReinstalling)
			return NO;
	}
	
	NSString *originalFullPath = [defaultPath stringByAppendingPathComponent: originalRelativeFilePath];
	
	if (![mainManager fileExistsAtPath: originalFullPath]) {
		NSLog(@"[%s] Nem létezik az originalFullPath!", __FUNCTION__);
		if (!self.uninstallingOrReinstalling) {			
			return NO;
		}
	}
	
	// azert csinaljuk ezt igy, mert ha nincs reversePath, akkor el kell menteni az eredetit
	NSString *arpf = reversePathFile;
	if (!arpf)
		arpf = @"";
	
	if (![arpf isEqualToString: @""]) {
		if (![mainManager fileExistsAtPath: reversePathFile]) {
			NSLog(@"[%s] Nem létezik az aReversePathFile!", __FUNCTION__);
			if (!self.uninstallingOrReinstalling) {
				return NO;
			}
		}
	}
	
	// akkor hozzaadjuk a visszaallitasi ponthoz
	NSDictionary *udict = [NSDictionary dictionaryWithObjectsAndKeys: arpf, fReversePatchFile, originalFullPath, fOriginalFilePath, originalFileSize, fOriginalFileSize, patchedFileSize, fPatchedFileSize, nil];
	[uninstallerPatchFiles addObject: udict];
	
	NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys: arpf, fReversePatchFile, patchFile, fPatchFile, originalFullPath, fOriginalFilePath, originalFileSize, fOriginalFileSize, patchedFileSize, fPatchedFileSize, nil];
	[filesToPatch addObject: dict];
	
	return YES;
}

/*!
 *			Hozzáadjuk az új fájlokat, és, hogy hova kell másolni őket.
 *			Ebben az esetben nem kell md5 hash, mert csak átmásoljuk, és uninstallnál töröljük.
 * \param	copyFilePath melyik fájlt akarjuk másolni.
 * \param	newRelativePath másolási mappa, melyik mappába akarjuk másolni a copyFilePath útvonalon lévő fájlt.
 * \pre		Érvényes fájl elérési útnak kell lennie az copyFilePath változónak.
 * \pre		Érvényes fájl elérési útnak kell lennie az newRelativePath változónak akkor, ha hozzátesszük a defaultPath végére.
 * \pre		Érvényes elérési útnak kell lennie a defaultPath változónak.
 * \return	Ha sikerült hozzáadni a listához.
 */
- (BOOL) addNewCopyFile: (NSString *) copyFilePath newRelativePath: (NSString *) newRelativePath {
	// ha nincs beallitva path, akkor kilepunk
	if (![self isValidDefaultPath]) {
		NSLog(@"[%s] Nem valós a defaultPath!", __FUNCTION__);
		return NO;
	}
	
	if (![mainManager fileExistsAtPath: copyFilePath]) {
		NSLog(@"[%s] Nem létezik az aCopyFilePath", __FUNCTION__);
		if (!self.uninstallingOrReinstalling) {
			return NO;
		}
	}
	
	NSString *newFileFullPath = [defaultPath stringByAppendingPathComponent: newRelativePath];
	if (![mainManager fileExistsAtPath: newFileFullPath]) {
		NSLog(@"[%s] Nem létezik az newFileFullPath mappa", __FUNCTION__);
		if (!self.uninstallingOrReinstalling) {
			return NO;
		}
	}
	
	newFileFullPath = [newFileFullPath stringByAppendingPathComponent: [copyFilePath lastPathComponent]];
	
	// hozzaadjuk az uninstallerhez, hogy tudjuk majd torolni
	NSDictionary *deleteObject = [NSDictionary dictionaryWithObjectsAndKeys: newFileFullPath, fNewFilePath, nil];
	[uninstallerDeleteFiles addObject: deleteObject];
	
	// hozzaadjuk a masolando objektumot a tarolohoz
	NSDictionary *copyObject = [NSDictionary dictionaryWithObjectsAndKeys: copyFilePath, fCopyFilePath, newFileFullPath, fNewFilePath, nil];
	[filesToCopy addObject: copyObject];
	
	return YES;
}

/*!
 *			Hozzáadjuk a kicserélendő fájlokat.
 * \param	newFilePath melyik fájlt szeretnénk cserélni.
 * \param	originalRelativePath melyik fájlra akarjuk cserélni.
 * \param	originalFileSize eredeti fájl mérete.
 * \pre		Érvényes fájl elérési útnak kell lennie.
 * \pre		Érvényes elérési útnak kell lennie a defaultPath változónak is.
 * \pre		Egyeznie kell a két elérési úton lévő fájl nevének.
 * \pre		originalFileSize paraméter nem lehet nil vagy 0 értékű.
 * \return	Ha sikerült hozzáadni a listához.
 */
- (BOOL) addNewReplaceFile: (NSString *) newFilePath originalRelativePath: (NSString *) originalRelativePath originalFileSize: (NSNumber *) originalFileSize {
	// ha nincs beallitva path, akkor kilepunk
	if (![self isValidDefaultPath]) {
		NSLog(@"[%s] Nem valós a defaultPath!", __FUNCTION__);
		return NO;
	}
	
	if (!originalFileSize || [originalFileSize unsignedLongLongValue] == 0) {
		NSLog(@"[%s] Nem érvényes az originalFileSize paraméter!", __FUNCTION__);
		if (!self.uninstallingOrReinstalling)
			return NO;
	}
	
	if (![mainManager fileExistsAtPath: newFilePath]) {
		NSLog(@"[%s] Nem létezik az aCopyFilePath", __FUNCTION__);
		if (!self.uninstallingOrReinstalling)
			return NO;
	}
	
	NSString *originalFileFullPath = [defaultPath stringByAppendingPathComponent: originalRelativePath];
	if (![mainManager fileExistsAtPath: originalFileFullPath]) {
		NSLog(@"[%s] Nem létezik az originalFileFullPath", __FUNCTION__);
		if (!self.uninstallingOrReinstalling) {
			return NO;
		}
	}
	
	// Itt pedig az tortenik, hogy hozzaadom az installerhez es az uninstallerhez is egyarant, mert ki kell cserelni vissza
	NSDictionary *replaceObject = [NSDictionary dictionaryWithObjectsAndKeys: newFilePath, fNewFilePath, originalFileFullPath, fOriginalFilePath, originalFileSize, fOriginalFileSize, nil];
	[filesToReplace addObject: replaceObject];
	
	NSDictionary *uninstallReplaceObject = [NSDictionary dictionaryWithObjectsAndKeys: newFilePath, fNewFilePath, originalFileFullPath, fOriginalFilePath, originalFileSize, fOriginalFileSize, nil];
	[uninstallerReplaceFiles addObject: uninstallReplaceObject];
	
	return YES;
}

#pragma mark - programs
/*!
 *			Elindítjuk a beállítasok szerint a patchelést, másolást, cserét, és az uninstall létrehozásat, ha kell
 *			Automatikusan ellenőrzi a fájl jogosultságokat, és átállítja admin jelszót kér, ha nem lehet valamelyiket írni.
 * \pre		A defaultPath mindenképpen egy létező mappára mutasson.
 * \pre		Mindenképpen hozzá kell adni vagy egy patch fájlt, vagy egy másoladnó fájlt, vagy egy kicserélendő fájlt!
 * \return	ha sikerült teljesen a futtatás, akkor tér vissza csak YES értékkel
 */
- (BOOL) install {
	// ha nincs beallitva path, akkor kilepunk
	if (![self isValidDefaultPath]) {
		NSLog(@"[%s] Nem valós a defaultPath!", __FUNCTION__);
		return NO;
	}
	
	// ha nincs beallitva semmilyen ertek, akkor nincs mit csinalni
	if ([filesToPatch count] == 0 && [filesToCopy count] == 0 && [filesToReplace count] == 0) {
		NSLog(@"[%s] Nincs egyetlen elem sem a tárolókban.", __FUNCTION__);
		return NO;
	}
	
	// ebben taroljuk, hogy melyik fajloka/mappak jogait szerenenk atallitani
	NSMutableDictionary *valtoztatottJoguFajlok = [NSMutableDictionary new];
	
	// backup mappat tartalmazo mappa ellenorzese
	{
		NSString *lastBackupPath = [backupFolderFullPath stringByDeletingLastPathComponent];
		if ([mainManager fileExistsAtPath: lastBackupPath] && ![mainManager isWritableFileAtPath: lastBackupPath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: lastBackupPath error: nil];
			if (!dict)
				NSLog(@"%s {Backup} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, lastBackupPath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: lastBackupPath];
		}
	}
	
	[filesToPatch enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *originalFilePath = [dict objectForKey: fOriginalFilePath];
		NSString *origFolderPath = [originalFilePath stringByDeletingLastPathComponent];
		// ellenorizni kell, hogy netan nincs-e mar az uj fajl helyen, mert ahhoz is hozza kell tudnunk ferni
		NSString *fileName = [originalFilePath lastPathComponent];
		NSString *newFilePath = [backupFolderFullPath stringByAppendingPathComponent: fileName];
		
		if ([mainManager fileExistsAtPath: originalFilePath] && ![mainManager isWritableFileAtPath: originalFilePath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: originalFilePath error: nil];
			if (!dict)
				NSLog(@"%s {filesToPatch, originalFilePath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, originalFilePath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: originalFilePath];
		}
		
		if ([mainManager fileExistsAtPath: origFolderPath] && ![mainManager isWritableFileAtPath: origFolderPath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: origFolderPath error: nil];
			if (!dict)
				NSLog(@"%s {filesToPatch, origFileFolder} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, origFolderPath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: origFolderPath];
		}
		
		if ([mainManager fileExistsAtPath: newFilePath] && ![mainManager isWritableFileAtPath: newFilePath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: newFilePath error: nil];
			if (!dict)
				NSLog(@"%s {filesToPatch, newFilePath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, newFilePath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: newFilePath];
		}
	}];
	
	[filesToCopy enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *newFilePath = [dict objectForKey: fNewFilePath];
		NSString *newFolderPath = [newFilePath stringByDeletingLastPathComponent];
		
		
		// itt a newFilePath-is le kell ellenorizni, mivel azt akarjuk majd eltavolitani
		if ([mainManager fileExistsAtPath: newFilePath] && ![mainManager isWritableFileAtPath: newFilePath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: newFilePath error: nil];
			if (!dict)
				NSLog(@"%s {filesToCopy, newFilePath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, newFilePath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: newFilePath];
		}
		
		if ([mainManager fileExistsAtPath: newFolderPath] && ![mainManager isWritableFileAtPath: newFolderPath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: newFolderPath error: nil];
			if (!dict)
				NSLog(@"%s {filesToCopy, newFolderPath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, newFolderPath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: newFolderPath];
		}
	}];
	
	[filesToReplace enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *originalFilePath = [dict objectForKey: fOriginalFilePath];
		NSString *origFolderPath = [originalFilePath stringByDeletingLastPathComponent];
		// az uj file helyet is le kell ellenorizni
		NSString *newFilePath = [backupFolderFullPath stringByAppendingPathComponent: [originalFilePath lastPathComponent]];
		
		if ([mainManager fileExistsAtPath: originalFilePath] && ![mainManager isWritableFileAtPath: originalFilePath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: originalFilePath error: nil];
			if (!dict)
				NSLog(@"%s {filesToReplace, originalFilePath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, originalFilePath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: originalFilePath];
		}
		
		if ([mainManager fileExistsAtPath: origFolderPath] && ![mainManager isWritableFileAtPath: origFolderPath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: origFolderPath error: nil];
			if (!dict)
				NSLog(@"%s {filesToReplace, origFileFolder} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, origFolderPath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: origFolderPath];
		}
		
		if ([mainManager fileExistsAtPath: newFilePath] && ![mainManager isWritableFileAtPath: newFilePath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: newFilePath error: nil];
			if (!dict)
				NSLog(@"%s {filesToReplace, newFilePath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, newFilePath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: newFilePath];
		}
	}];
	
	if ([valtoztatottJoguFajlok count] > 0) {
		// johet a jogok atallitasa
		NSMutableArray *arguments = [NSMutableArray new];
		
		[valtoztatottJoguFajlok enumerateKeysAndObjectsUsingBlock: ^(NSString *path, NSNumber *permission, BOOL *stop) {
			[arguments addObject: path];
			[arguments addObject: [NSString stringWithFormat: @"%i", 777]];
		}];
		
		NSString *error = nil;
		NSString *output = nil;
		if ([self runProcessAsAdministrator: [[NSBundle mainBundle] pathForResource: @"permissions" ofType: @"sh"]
							  withArguments: arguments
									 output: &output
						   errorDescription: &error]) {
			
			if (error)
				NSLog(@"%s {run ok} AppleScript error: %@", __FUNCTION__, error);
			
			if (!output) {
				NSLog(@"%s {run ok} Nincs output a scriptnél", __FUNCTION__);
			}
			else if(![output isEqualToString: @"ok"]) {
				NSLog(@"%s {run ok} Hiba a shell script futtatásánál: %@", __FUNCTION__, output);
			}
		}
		else {
			NSLog(@"%s {run fail} Nem futott le a shell script", __FUNCTION__);
			
			if (error)
				NSLog(@"%s {run fail} AppleScript error: %@", __FUNCTION__, error);
			
			if (output) {
				NSLog(@"%s {run fail} Output a scriptnél: %@", __FUNCTION__, output);
			}
		}
		
		[arguments release];
	}
	
	// letrehozzuk most mar a backup mappat
	if (![self createBackupFolderWithRelativePath])
		return NO;
	
	__block BOOL fail = NO;
	// eloszor a patchelest csinaljuk meg
	[filesToPatch enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *patchFilePath = [dict objectForKey: fPatchFile];
		NSString *originalFilePath = [dict objectForKey: fOriginalFilePath];
		NSNumber *originalFileSize = [dict objectForKey: fOriginalFileSize];
		
		NSString *fileName = [originalFilePath lastPathComponent];
		NSString *newFilePath = [backupFolderFullPath stringByAppendingPathComponent: fileName];
		NSNumber *patchedFileSize = [dict objectForKey: fPatchedFileSize];
		
		NSString *reversePath = [dict objectForKey: fReversePatchFile];
		
		BOOL originalFileVan = [mainManager fileExistsAtPath: originalFilePath];
		BOOL newFileVan = [mainManager fileExistsAtPath: newFilePath];
		BOOL reversePathFileVan = [mainManager fileExistsAtPath: reversePath];
		
		if (!originalFileVan && !newFileVan) {
			NSLog(@"[%s] {Patchelés} Nem létezik sem az eredeti fájl, sem a backup fájl: %@.", __FUNCTION__, newFilePath);
			fail = YES;
		}
		else {
			BOOL installSucces = NO;
			NSDictionary *originalFileAttributes = [mainManager attributesOfItemAtPath: originalFilePath error: nil];
			
			if (originalFileVan) {
				if ([[originalFileAttributes objectForKey: NSFileSize] isEqualToNumber: patchedFileSize]) {
					NSLog(@"[%s] {Patchelés} Patchelt fájl már létezik az eredeti helyén, ezért nem csinálunk most semmit sem: %@.", __FUNCTION__, newFilePath);
					installSucces = YES;
				}
			}
			
			if (!installSucces) {
				BOOL backupFileAlredyExists = NO;
				
				if (newFileVan) {
					NSDictionary *newFileAttributes = [mainManager attributesOfItemAtPath: newFilePath error: nil];
					if ([[newFileAttributes objectForKey: NSFileSize] isEqualToNumber: originalFileSize]) {
						NSLog(@"[%s] {Patchelés} Patchelendő (eredeti) fájl már létezik, de nem töröljük, mert jó ez a fájl: %@.", __FUNCTION__, newFilePath);
						backupFileAlredyExists = YES;
					}
					else if ([[newFileAttributes objectForKey: NSFileSize] isEqualToNumber: patchedFileSize]) {
						
						NSLog(@"[%s] {Patchelés} Patchelt fájl már létezik, megpróbáljuk áthelyezni, mert ez már a telepíteni kívánt fájl: %@.", __FUNCTION__, newFilePath);
						
						// ha az eredeti helyen van az eredeti fajl, akkor megprobaljuk biztonsagba menteni
						BOOL kellVisszanevezni = NO;
						NSString *toldalek = @"_backup";
						if (originalFileVan && [[originalFileAttributes objectForKey: NSFileSize] isEqualToNumber: originalFileSize] && !reversePathFileVan) {
							if (![mainManager moveItemAtPath: originalFilePath toPath: [newFilePath stringByAppendingString: toldalek] error: nil]) {
								NSLog(@"[%s] {Patchelés} Nem sikerült áthelyezni és átnevezni az eredeti fájlt a biztonsági mentéshez: %@.", __FUNCTION__, originalFilePath);
								// ettol meg nem all meg a vilag...
							}
							else {
								kellVisszanevezni = YES;
								originalFileVan = NO;
							}
						}
						
						if (originalFileVan && ![mainManager removeItemAtPath: originalFilePath error: nil]) {
							NSLog(@"[%s] {Patchelés} Nem sikerült az eredeti fájl törlése, hogy áthelyezzük a backupban lévőt: %@.", __FUNCTION__, originalFilePath);
							fail = YES;
							*stop = YES;
						}
						else if (![mainManager moveItemAtPath: newFilePath toPath: originalFilePath error: nil]) {
							NSLog(@"[%s] {Patchelés} Nem sikerült áthelyezni a fájlt: %@.", __FUNCTION__, originalFilePath);
							fail = YES;
							*stop = YES;
						}
						else {
							installSucces = YES;
							
							if (kellVisszanevezni) {
								if (![mainManager moveItemAtPath: [newFilePath stringByAppendingString: toldalek] toPath: newFilePath error: nil]) {
									NSLog(@"[%s] {Patchelés} Nem sikerült az eredeti fájlt a biztonsági mentésének visszanevezése: %@.", __FUNCTION__, [newFilePath stringByAppendingString: toldalek]);
									// ettol meg nem all meg a vilag...
								}
								
								/* ebben az esetben biztos, hogy nem kell eltavolitani, mert nincs reverse path file
								// csak akkor kell eltavolitani, ha van reverse patch fajl, es van eredeti fajlunk is
								if (reversePathFileVan)
									// hozzaadjuk az eltavolitandok listajarol, ha sikerult a telepites
									[filesToDeleteAfterSuccesFullInstall addObject: newFilePath];
								*/
							}
						}
					}
					else {
						NSLog(@"[%s] {Patchelés} Patchelendő fájl már létezik a backupban, ezért megpróbáljuk törölni: %@.", __FUNCTION__, newFilePath);
						if (![mainManager removeItemAtPath: newFilePath error: nil]) {
							NSLog(@"[%s] {Patchelés} Nem sikerült az valamikor előzőleg létrehozott fájl törlése a backupból: %@.", __FUNCTION__, newFilePath);
							fail = YES;
							*stop = YES;
						}
					}
				}
				
				if (!fail && !installSucces) {
					if (!fail && !originalFileVan && !backupFileAlredyExists) {
						NSLog(@"[%s] {Patchelés} Nem létezik a patchelni kívánt fájl: %@.", __FUNCTION__, originalFilePath);
						fail = YES;
						*stop = YES;
					}
					
					if (!fail && ![mainManager fileExistsAtPath: patchFilePath]) {
						NSLog(@"[%s] {Patchelés} Nem létezik a patch fájl: %@.", __FUNCTION__, patchFilePath);
						fail = YES;
						*stop = YES;
					}
					
					// ha mar letezik a backup fajl, akkor nem akarunk mozgatni, hanem torolni az eredetit
					if (!fail && originalFileVan && backupFileAlredyExists) {
						if (![mainManager removeItemAtPath: originalFilePath error: nil]) {
							NSLog(@"[%s] {Patchelés} Nem sikerült az eredeti fájl törlése: %@.", __FUNCTION__, originalFilePath);
							fail = YES;
							*stop = YES;
						}
					}
					
					if (!fail && originalFileVan && ![mainManager moveItemAtPath: originalFilePath toPath: newFilePath error: nil]) {
						NSLog(@"[%s] {Patchelés} Nem sikerült áthelyezni a fájlt: %@.", __FUNCTION__, originalFilePath);
						fail = YES;
						*stop = YES;
					}
					
					if (!fail) {
						// ha nincs reversePath file, akkor meghagyjuk a backupban, mert onnan kell majd athelyezessel visszaallitani
						
						if (reversePathFileVan)
						// hozzaadjuk az eltavolitandok listajarol, ha sikerult a telepites
							[filesToDeleteAfterSuccesFullInstall addObject: newFilePath];
						
						NSString *errorMessage = [XDeltaAdapter ApplyPatch: patchFilePath toFile: newFilePath andCreate: originalFilePath];
						if (errorMessage != nil) {
							NSLog(@"[%s] {Patchelés} Nem sikerült a patch fájl létrehozása;\n\tHibaüzenet: %@\n\tpatch fájl: %@\n\teredeti fájl: %@.", __FUNCTION__, errorMessage, patchFilePath, originalFilePath);
							fail = YES;
							*stop = YES;
						}
					}
				}
			}
		}
	}];

	if (fail) {
		return NO;
	}
	
	[filesToCopy enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *originalFilePath = [dict objectForKey: fCopyFilePath];
		NSString *newFilePath = [dict objectForKey: fNewFilePath];
		
		BOOL originalFileVan = [mainManager fileExistsAtPath: originalFilePath]; // bar ennek nem sok ertelme van, mert elvileg kell, hogy letezzen. Mikor hozzaadtuk leellenoriztuk...
		
		BOOL succesfullInstall = NO;
		if ([mainManager fileExistsAtPath: newFilePath]) {
			NSLog(@"[%s] {Másolás} Másoldandó fájl új helye már létezik: %@.", __FUNCTION__, newFilePath);
			NSDictionary *originalFileAttributes = [mainManager attributesOfItemAtPath: originalFilePath error: nil];
			NSDictionary *newFileAttributes = [mainManager attributesOfItemAtPath: newFilePath error: nil];
			
			if ([[originalFileAttributes objectForKey: NSFileSize] isEqualToNumber: [newFileAttributes objectForKey: NSFileSize]]) {
				NSLog(@"[%s] {Másolás} Másoldandó fájl már a helyén volt, így nincs mit tenni: %@.", __FUNCTION__, newFilePath);
				succesfullInstall = YES;
			}
			else {
				NSLog(@"[%s] {Másolás} Másoldandó fájl új helyén már létezik valamilyen fájl, ezért megpróbáljuk törölni: %@.", __FUNCTION__, newFilePath);
				if (![mainManager removeItemAtPath: newFilePath error: nil]) {
					NSLog(@"[%s] {Másolás} Nem sikerült az valamikor előzőleg létrehozott fájl törlése: %@.", __FUNCTION__, newFilePath);
					fail = YES;
					*stop = YES;
				}
			}
		}
		
		if (!fail && !succesfullInstall) {
			if (!originalFileVan) {
				NSLog(@"[%s] {Másolás} Nem létezik a másolni kívánt fájl: %@.", __FUNCTION__, originalFilePath);
				fail = YES;
				*stop = YES;
			}
			else if (![mainManager copyItemAtPath: originalFilePath toPath: newFilePath error: nil]) {
				NSLog(@"[%s] {Másolás} Nem sikerült másolni a fájlt: %@.", __FUNCTION__, originalFilePath);
				fail = YES;
				*stop = YES;
			}
		}
	}];
		
	
	if (fail) {
		return NO;
	}
	
	[filesToReplace enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *newFilePath = [dict objectForKey: fNewFilePath]; // ezt a fajlt akarjuk kicserelni, az installerben levo fajl
		NSString *originalFilePath = [dict objectForKey: fOriginalFilePath]; // erre a fajlra akarjuk kicserelni
		
		NSDictionary *origAttr = [mainManager attributesOfItemAtPath: originalFilePath error: nil];
		NSDictionary *newFileAttributes = [mainManager attributesOfItemAtPath: newFilePath error: nil];
		
		BOOL originalFileVan = [mainManager fileExistsAtPath: originalFilePath];
		
		// elso korben leellenprizzuk, hogy nincs-e mar a helyen a fajl
		if (originalFileVan && [[newFileAttributes objectForKey: NSFileSize] isEqualToNumber: [origAttr objectForKey: NSFileSize]]) {
			NSLog(@"[%s] {Cserélés} Már a kicserélendő fájl a helyén van, ez kész: %@.", __FUNCTION__, newFilePath); // backupot nem tudunk létrehozni, mert ki tudja, hogy hol van a backup. Ha szerencsénk van, akkor a backup mappában :D
		}
		else {
			if (![[newFilePath lastPathComponent] isEqualToString: [originalFilePath lastPathComponent]]) {
				NSLog(@"[%s] {Cserélés} Nem egyezik a két cserélendő fájl: eredeti fájlnév: %@, új fájl név: %@.", __FUNCTION__, [originalFilePath lastPathComponent], [newFilePath lastPathComponent]);
				fail = YES;
				*stop = YES;
			}
			
			if (!fail && ![mainManager fileExistsAtPath: newFilePath]) {
				NSLog(@"[%s] {Cserélés} Nem létezik a cserélendő fájl: %@.", __FUNCTION__, newFilePath);
				fail = YES;
				*stop = YES;
			}
		
			NSNumber *originalFileSize = [dict objectForKey: fOriginalFileSize];
			NSString *backupFilePath = [backupFolderFullPath stringByAppendingPathComponent: [newFilePath lastPathComponent]];
			
			BOOL backupSucces = NO;
			
			// megnézzük, hogy nem létezik-e már a fájl, ahová másolni akarjuk, és ha létezik, akkor megpróbáljuk törölni, ha kell.
			if (!fail && [mainManager fileExistsAtPath: backupFilePath]) {
				// letezik a backup file, megviszgaljuk, hogy az eredeti-e, vagy csak valami rossz fajlt, es toroljuk
				NSLog(@"[%s] {Cserélés} A backup már létezik: %@.", __FUNCTION__, backupFilePath);
				NSDictionary *backupAttributes = [mainManager attributesOfItemAtPath: backupFilePath error: nil];
				
				if ([[backupAttributes objectForKey: NSFileSize] isEqualToNumber: originalFileSize]) {
					NSLog(@"[%s] {Cserélés} Az eredeti fájl backupja már létezik: %@.", __FUNCTION__, backupFilePath);
					backupSucces = YES;
				}
				else {
					NSLog(@"[%s] {Cserélés} Az eredeti fájl backupja már létezik, ezért megpróbáljuk törölni: %@.", __FUNCTION__, backupFilePath);
					if (![mainManager removeItemAtPath: backupFilePath error: nil]) {
						NSLog(@"[%s] {Cserélés} Nem sikerült az valamikor előzőleg létrehozott backup fájl törlése: %@.", __FUNCTION__, backupFilePath);
						fail = YES;
						*stop = YES;
					}
				}
			}
			
			// eredeti fajlt atmozgatjuk a backup mappaba
			if (!fail && !backupSucces) {
				if (!originalFileVan) {
					NSLog(@"[%s] {Cserélés} Nem létezik az eredeti fájl: %@.", __FUNCTION__, originalFilePath);
				}
				else if (![mainManager moveItemAtPath: originalFilePath toPath: backupFilePath error: nil]) {
					NSLog(@"[%s] {Cserélés} Nem sikerült áthelyezni a fájlt: %@.", __FUNCTION__, originalFilePath);
					fail = YES;
					*stop = YES;
				}
				else {
					// sikeresen atmozgattuk az eredeti fajlt, igy kesobb biztosan nem kell menteni
					originalFileVan = NO;
				}
			}
			
			// tehat van backup fajlunk, de az eredeti nem biztos, hogy eltavolitasra kerult
			if (!fail && originalFileVan && ![mainManager removeItemAtPath: originalFilePath error: nil]) {
				NSLog(@"[%s] {Cserélés} Nem sikerült az valamikor előzőleg létrehozott backup fájl törlése: %@.", __FUNCTION__, originalFilePath);
				fail = YES;
				*stop = YES;
			}
			
			// uj fajlt atmozgatjuk a regi heylere
			if (!fail && ![mainManager copyItemAtPath: newFilePath toPath: originalFilePath error: nil]) {
				NSLog(@"[%s] {Cserélés} Nem sikerült áthelyezni az új fájlt: %@.", __FUNCTION__, newFilePath);
				fail = YES;
				*stop = YES;
			}
		}
	}];

	if (fail) {
		return NO;
	}
	
	// eltavolitjuk azokat a fajlokat, amiket kesobb vissza tudunk allitani (pl: patch)
	[filesToDeleteAfterSuccesFullInstall enumerateObjectsUsingBlock: ^(NSString *filePath, NSUInteger idx, BOOL *stop) {
		if (![mainManager removeItemAtPath: filePath error: nil])
			NSLog(@"[%s] {succesfull delete} Nem sikerült a fájl törlése: %@.", __FUNCTION__, filePath);
	}];
	
	// ha esetleg mégsem kértünk mentést, akkor kitöröljük a backup mappát teljesen
	if (!uninstallerEnabled) {
		// backup mappa eltavolitasa
		if ([mainManager fileExistsAtPath: backupFolderFullPath]) {
			if (![mainManager removeItemAtPath: backupFolderFullPath error: nil])
				NSLog(@"[%s] Nem sikerült a backup mappát törölni: %@.", __FUNCTION__, backupFolderFullPath);
		}
		else {
			NSLog(@"[%s] Nem létezik a backup mappa: %@.", __FUNCTION__, backupFolderFullPath);
		}
	}
	
	// visszaallitjuk a fajljogokat, mar ha van mit visszaallitani
	if ([valtoztatottJoguFajlok count] > 0) {
		// johet a jogok atallitasa
		NSMutableArray *arguments = [NSMutableArray new];
		
		[valtoztatottJoguFajlok enumerateKeysAndObjectsUsingBlock: ^(NSString *path, NSNumber *permission, BOOL *stop) {
			[arguments addObject: path];
			[arguments addObject: [NSString stringWithFormat: @"%o", [permission shortValue]]];
		}];
		
		NSString *error = nil;
		NSString *output = nil;
		if ([self runProcessAsAdministrator: [[NSBundle mainBundle] pathForResource: @"permissions" ofType: @"sh"]
							  withArguments: arguments
									 output: &output
						   errorDescription: &error]) {
			
			if (error)
				NSLog(@"%s {back run ok} AppleScript error: %@", __FUNCTION__, error);
			
			if (!output) {
				NSLog(@"%s {back run ok} Nincs output a scriptnél", __FUNCTION__);
			}
			else if(![output isEqualToString: @"ok"]) {
				NSLog(@"%s {back run ok} Hiba a shell script futtatásánál: %@", __FUNCTION__, output);
			}
		}
		else {
			NSLog(@"%s {back run fail} Nem futott le a shell script", __FUNCTION__);
			
			if (error)
				NSLog(@"%s {back run fail} AppleScript error: %@", __FUNCTION__, error);
			
			if (output) {
				NSLog(@"%s {back run fail} Output a scriptnél: %@", __FUNCTION__, output);
			}
		}
		
		[arguments release];
	}
	
	[valtoztatottJoguFajlok release];
	
	return YES;
}


/*!
 *			Elindítjuk a beállítasok szerint a visszaállítást.
 * \pre		A defaultPath mindenképpen egy létező mappára mutasson!
 * \pre		Mindenképpen hozzá kell adni vagy egy patch fájlt, vagy egy másoladnó fájlt, vagy egy kicserélendő fájlt!
 * \pre		Be kell legyen kapcsolva az uninstallerEnabled opció!
 * \return	ha sikerült teljesen a futtatás, akkor tér vissza csak YES értékkel
 */
- (BOOL) uninstall {
	if (!uninstallerEnabled) {
		NSLog(@"[%s] Uninstall nincs engedélyezve.", __FUNCTION__);
		return NO;
	}
	
	// ebben taroljuk, hogy melyik fajloka/mappak jogait szerenenk atallitani
	NSMutableDictionary *valtoztatottJoguFajlok = [NSMutableDictionary new];
	
	// backup mappat tartalmazo mappa ellenorzese
	{
		NSString *lastBackupPath = [backupFolderFullPath stringByDeletingLastPathComponent];
		if ([mainManager fileExistsAtPath: lastBackupPath] && ![mainManager isWritableFileAtPath: lastBackupPath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: lastBackupPath error: nil];
			if (!dict)
				NSLog(@"%s {Backup} Talán nincs jogunk olvasni a mappa attributumait: %@", __FUNCTION__, lastBackupPath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: lastBackupPath];
		}
		
		// itt a backup mappat is leellenorizzuk, mert lehet azt sem lehet irni
		if ([mainManager fileExistsAtPath: backupFolderFullPath] && ![mainManager isWritableFileAtPath: backupFolderFullPath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: backupFolderFullPath error: nil];
			if (!dict)
				NSLog(@"%s {Backup} Talán nincs jogunk olvasni a backup mappa attributumait: %@", __FUNCTION__, backupFolderFullPath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: backupFolderFullPath];
		}
	}
	
	[uninstallerPatchFiles enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *originalFilePath = [dict objectForKey: fOriginalFilePath];
		NSString *origFolderPath = [originalFilePath stringByDeletingLastPathComponent];
		
		NSString *fileName = [originalFilePath lastPathComponent];
		NSString *newFilePath = [backupFolderFullPath stringByAppendingPathComponent: fileName];
		
		if ([mainManager fileExistsAtPath: originalFilePath] && ![mainManager isWritableFileAtPath: originalFilePath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: originalFilePath error: nil];
			if (!dict)
				NSLog(@"%s {uninstallerPatchFiles, originalFilePath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, originalFilePath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: originalFilePath];
		}
		
		if ([mainManager fileExistsAtPath: origFolderPath] && ![mainManager isWritableFileAtPath: origFolderPath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: origFolderPath error: nil];
			if (!dict)
				NSLog(@"%s {uninstallerPatchFiles, origFileFolder} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, origFolderPath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: origFolderPath];
		}
		
		if ([mainManager fileExistsAtPath: newFilePath] && ![mainManager isWritableFileAtPath: newFilePath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: newFilePath error: nil];
			if (!dict)
				NSLog(@"%s {uninstallerPatchFiles, newFilePath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, newFilePath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: newFilePath];
		}
	}];
	
	[uninstallerDeleteFiles enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *newFilePath = [dict objectForKey: fNewFilePath];
		NSString *newFolderPath = [newFilePath stringByDeletingLastPathComponent];
		
		// itt a newFilePath-is le kell ellenorizni, mivel azt akarjuk majd eltavolitani
		if ([mainManager fileExistsAtPath: newFilePath] && ![mainManager isWritableFileAtPath: newFilePath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: newFilePath error: nil];
			if (!dict)
				NSLog(@"%s {uninstallerDeleteFiles, newFilePath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, newFilePath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: newFilePath];
		}
		
		if ([mainManager fileExistsAtPath: newFolderPath] && ![mainManager isWritableFileAtPath: newFolderPath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: newFolderPath error: nil];
			if (!dict)
				NSLog(@"%s {uninstallerDeleteFiles, newFolderPath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, newFolderPath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: newFolderPath];
		}
	}];
	
	[uninstallerReplaceFiles enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *originalFilePath = [dict objectForKey: fOriginalFilePath];
		NSString *origFolderPath = [originalFilePath stringByDeletingLastPathComponent];
		NSString *newFilePath = [backupFolderFullPath stringByAppendingPathComponent: [originalFilePath lastPathComponent]];
		// itt most a mappat nem kell ellenorizni, mert mar az az elobb megtettuk
		
		if ([mainManager fileExistsAtPath: originalFilePath] && ![mainManager isWritableFileAtPath: originalFilePath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: originalFilePath error: nil];
			if (!dict)
				NSLog(@"%s {uninstallerReplaceFiles, originalFilePath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, originalFilePath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: originalFilePath];
		}
		
		if ([mainManager fileExistsAtPath: origFolderPath] && ![mainManager isWritableFileAtPath: origFolderPath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: origFolderPath error: nil];
			if (!dict)
				NSLog(@"%s {uninstallerReplaceFiles, origFileFolder} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, origFolderPath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: origFolderPath];
		}
		
		if ([mainManager fileExistsAtPath: newFilePath] && ![mainManager isWritableFileAtPath: newFilePath]) {
			NSDictionary *dict = [mainManager attributesOfItemAtPath: newFilePath error: nil];
			if (!dict)
				NSLog(@"%s {uninstallerReplaceFiles, newFilePath} Talán nincs jogunk olvasni a fájl attributumait: %@", __FUNCTION__, newFilePath);
			else
				[valtoztatottJoguFajlok setObject: (NSNumber *)[dict objectForKey: NSFilePosixPermissions] forKey: newFilePath];
		}
	}];
	
	if ([valtoztatottJoguFajlok count] > 0) {
		// johet a jogok atallitasa
		NSMutableArray *arguments = [NSMutableArray new];
		
		[valtoztatottJoguFajlok enumerateKeysAndObjectsUsingBlock: ^(NSString *path, NSNumber *permission, BOOL *stop) {
			[arguments addObject: path];
			[arguments addObject: [NSString stringWithFormat: @"%i", 777]];
		}];
		
		NSString *error = nil;
		NSString *output = nil;
		if ([self runProcessAsAdministrator: [[NSBundle mainBundle] pathForResource: @"permissions" ofType: @"sh"]
							  withArguments: arguments
									 output: &output
						   errorDescription: &error]) {
			
			if (error)
				NSLog(@"%s {run ok} AppleScript error: %@", __FUNCTION__, error);
			
			if (!output) {
				NSLog(@"%s {run ok} Nincs output a scriptnél", __FUNCTION__);
			}
			else if(![output isEqualToString: @"ok"]) {
				NSLog(@"%s {run ok} Hiba a shell script futtatásánál: %@", __FUNCTION__, output);
			}
		}
		else {
			NSLog(@"%s {run fail} Nem futott le a shell script", __FUNCTION__);
			
			if (error)
				NSLog(@"%s {run fail} AppleScript error: %@", __FUNCTION__, error);
			
			if (output) {
				NSLog(@"%s {run fail} Output a scriptnél: %@", __FUNCTION__, output);
			}
		}
		
		[arguments release];
	}
	
	__block BOOL vanfail = false;
	
	// eloszor a patchelest csinaljuk meg
	[uninstallerPatchFiles enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *originalFilePath = [dict objectForKey: fOriginalFilePath];
		NSNumber *originalFileSize = [dict objectForKey: fOriginalFileSize];
		NSDictionary *originalFileAttributes = [mainManager attributesOfItemAtPath: originalFilePath error: nil];
		
		NSString *fileName = [originalFilePath lastPathComponent];
		NSString *newFilePath = [backupFolderFullPath stringByAppendingPathComponent: fileName];
		
		BOOL origFileVan = [mainManager fileExistsAtPath: originalFilePath];
		BOOL newFileVan = [mainManager fileExistsAtPath: newFilePath];
		
		if (origFileVan && [[originalFileAttributes objectForKey: NSFileSize] isEqualToNumber: originalFileSize]) {
			NSLog(@"[%s] {Patchelés} Az eredetei fájl a helyén van: %@.", __FUNCTION__, originalFilePath);
			if (newFileVan) {
				if (![mainManager removeItemAtPath: newFilePath error: nil]) {
					NSLog(@"[%s] {Patchelés} Nem sikerült törölni a backup fájlt: %@.", __FUNCTION__, newFilePath);
				}
			}
		}
		else {
			NSString *patchFilePath = [dict objectForKey: fReversePatchFile];
			
			BOOL vanPathFile = YES;
			if (!patchFilePath || [patchFilePath isEqualToString: @""] || ![mainManager fileExistsAtPath: patchFilePath]) {
				NSLog(@"[%s] {Patchelés} Nem létezik a patch fájl: %@.", __FUNCTION__, patchFilePath);
				vanPathFile = NO;
			}
			
			if (!vanPathFile && !newFileVan) {
				// tehat nincs sem patch fileunk, sem amibol vissza tudnank allitani
				NSLog(@"[%s] {Patchelés} Nem létezik az eredeti fájl sem a backupban: %@.", __FUNCTION__, newFilePath);
				// patch filerol mar irtunk az elobb, azert nem kell rola nslog
				vanfail = YES;
			}
			else {
				NSNumber *patchedFileSize = [dict objectForKey: fPatchedFileSize];
				BOOL mehetaPath = NO;
				BOOL backupKesz = NO;
				
				if (newFileVan) {
					// itt meg nem biztos, hogy hibas a rendszer, mert lehet, hogy csak athelyezni kell az eredeti fajlt
					NSDictionary *backupFileAttributes = [mainManager attributesOfItemAtPath: newFilePath error: nil];
					if ([[backupFileAttributes objectForKey: NSFileSize] isEqualToNumber: originalFileSize]) {
						// tehat az eredeti fajl a backupban talalhato
						// eloszor toroljuk a fajlt, az eredeti helyen, majd ha sikerult, akkor atmozgatjuk az eredeti fajlt
						if (origFileVan && ![mainManager removeItemAtPath: originalFilePath error: nil]) {
							NSLog(@"[%s] {Patchelés} Nem sikerült törölni a patchelt fájlt az eredti helyen: %@.", __FUNCTION__, originalFilePath);
							vanfail = YES;
						}
						else if (![mainManager moveItemAtPath: newFilePath toPath: originalFilePath error: nil]) {
							NSLog(@"[%s] {Patchelés} Nem sikerült az eredeti fájlt a backupból átmozgatni az eredeti helyére: %@.", __FUNCTION__, newFilePath);
							vanfail = YES;
						}
						else {
							// tehat sikerult athelyezni az eredeti fajlt, nincs tovabbi teendonk
							backupKesz = YES;
						}
					}
					else if ([[backupFileAttributes objectForKey: NSFileSize] isEqualToNumber: patchedFileSize]) {
						// nemtom hogy a fenebe kerult ide, de a lenyeg, hogy a patchelendo fajl mar a backup mappaban van.
						mehetaPath = YES;
						NSLog(@"[%s] {Patchelés} A patchelendő fájl már a backupban van: %@.", __FUNCTION__, newFilePath);
					}
				}
				
				if (!backupKesz && !mehetaPath) {
					// tehat patchelni kellene a fajlt, es nem a megfelelo van a backupban
					// elvileg letezik a patchfile, mert ha nem, akkor mar hamarabb uzenetet kuldtunk, mert a backup file sem letezik
					// egyedul az origfilepatch-et kell leellenorizni, hogy file meretben rendben van-e (nem lehet az orig helyen az orig file, mert mar az elejen ezt kiszurtuk)
					
					
					NSDictionary *patchedFileAttributes = [mainManager attributesOfItemAtPath: originalFilePath error: nil];
					
					// eloszor eltavolitjuk a backupban levot, ha ott van egyaltalan valami
					if (newFileVan && ![mainManager removeItemAtPath: newFilePath error: nil]) {
						NSLog(@"[%s] {Patchelés} Nem sikerült törölni a backupban lévő fájlt: %@.", __FUNCTION__, newFilePath);
					}
					else if (!origFileVan) {
						NSLog(@"[%s] {Patchelés} Nem létezik a patchelni kívánt fájl: %@.", __FUNCTION__, originalFilePath);
						vanfail = YES;
					}
					else if (![[patchedFileAttributes objectForKey: NSFileSize] isEqualToNumber: patchedFileSize]) {
						NSLog(@"[%s] {Patchelés} Nem egyezik a patchelni kívánt fájl mérete: %@.", __FUNCTION__, originalFilePath);
						vanfail = YES;
					}
					else if (![mainManager moveItemAtPath: originalFilePath toPath: newFilePath error: nil]) {
						NSLog(@"[%s] {Patchelés} Nem sikerült áthelyezni a patchelni kívánt fájlt: %@.", __FUNCTION__, originalFilePath);
						vanfail = YES;
					}
					else {
						mehetaPath = YES;
					}
				}
				
				if (mehetaPath && vanPathFile) {
					// elvileg most mar minden a legnagyobb rendben, kezdhetjuk a patchelest
					NSString *errorMessage = [XDeltaAdapter ApplyPatch: patchFilePath toFile: newFilePath andCreate: originalFilePath];
					if (errorMessage != nil) {
						NSLog(@"[%s] {Patchelés} Nem sikerült a patch fájl létrehozása;\n\tHibaüzenet: %@\n\tpatch fájl: %@\n\teredeti fájl: %@", __FUNCTION__, errorMessage, patchFilePath, originalFilePath);
						vanfail = YES;
					}
				}
			}
		}
	}];
	
	[uninstallerDeleteFiles enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		NSString *newFilePath = [dict objectForKey: fNewFilePath];
		
		if (![mainManager fileExistsAtPath: newFilePath]) {
			NSLog(@"[%s] {Törlés} Nem létezik a törölni kívánt fájl: %@.", __FUNCTION__, newFilePath);
			// ha nem letezik, attol meg ne allitsuk le a program futasat...
		} else if (![mainManager removeItemAtPath: newFilePath error: nil]) {
			NSLog(@"[%s] {Törlés} Nem sikerült törölni a fájl: %@.", __FUNCTION__, newFilePath);
			vanfail = YES;
		}
	}];
	
	[uninstallerReplaceFiles enumerateObjectsUsingBlock: ^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
		BOOL fail = false;
		
		// elso korben leellenorizzuk, hogy az eredeti fajl nincs-e a helyen
		NSString *originalFilePath = [dict objectForKey: fOriginalFilePath];
		BOOL originalFileVan = [mainManager fileExistsAtPath: originalFilePath];
		BOOL succesUninstall = NO;
		if (!originalFileVan) {
			NSLog(@"[%s] {Cserélés} Nem létezik az eredeti helyen lévő fájl: %@.", __FUNCTION__, originalFilePath);
		}
		else {
			NSDictionary *originalFileAttributes = [mainManager attributesOfItemAtPath: originalFilePath error: nil];
			NSNumber *originalFileSize = [dict objectForKey: fOriginalFileSize];
			if ([[originalFileAttributes objectForKey: NSFileSize] isEqualToNumber: originalFileSize]) {
				NSLog(@"[%s] {Cserélés} Eredeti fájl az eredeti helyen van: %@.", __FUNCTION__, originalFilePath);
				succesUninstall = YES;
			}
		}
		
		if (!succesUninstall) {
			
			NSString *fileName = @"";
			
			if (!originalFileVan) {
				NSString *newFilePath = [dict objectForKey: fNewFilePath];
				if (![mainManager fileExistsAtPath: newFilePath]) {
					NSLog(@"[%s] {Cserélés} Nem létezik a sem az eredeti fájl, sem a cserélendő fájl, így nem tudunk backup nevet számolni: %@.", __FUNCTION__, newFilePath);
					fail = YES;
					vanfail = YES;
				}
				else {
					fileName = [newFilePath lastPathComponent];
				}
			}
			else {
				fileName = [originalFilePath lastPathComponent];
			}
			
			NSString *backupFilePath = [backupFolderFullPath stringByAppendingPathComponent: fileName];
			
			if (!fail && ![mainManager fileExistsAtPath: backupFilePath]) {
				NSLog(@"[%s] {Cserélés} Nem létezik a backup fájl: %@.", __FUNCTION__, backupFilePath);
				fail = YES;
				vanfail = YES;
			}
			
			// most megprobaljuk eltavolitani a mostani fajlt, hogy oda tudjuk masolni az eredeti fajlt
			if (!fail && originalFileVan && [mainManager fileExistsAtPath: originalFilePath]) {
				// csak leteznie kell amit ki akarunk cserelni... szoval ne dobjon uzeneteket
				if (![mainManager removeItemAtPath: originalFilePath error: nil]) {
					NSLog(@"[%s] {Cserélés} Nem sikerült a valamikor előzőleg létrehozott fájl törlése: %@.", __FUNCTION__, originalFilePath);
					fail = YES;
					vanfail = YES;
				}
			}
			else {
				if (!fail)
				// ha nem letezik, akkor sem allunk le, ugy is az eredetit kell ide athelyezni
					NSLog(@"[%s] {Cserélés} Nem létezik a kicserélendő fájl: %@.", __FUNCTION__, originalFilePath);
			}
			
			// eredeti fajlt atmozgatjuk a backup mappaba
			if (!fail && ![mainManager moveItemAtPath: backupFilePath toPath: originalFilePath error: nil]) {
				NSLog(@"[%s] {Cserélés} Nem sikerült áthelyezni a backup fájlt: %@.", __FUNCTION__, backupFilePath);
				vanfail = YES;
			}
		}
	}];
	
	// backup mappa eltavolitasa
	if ([mainManager fileExistsAtPath: backupFolderFullPath]) {
		if (![mainManager removeItemAtPath: backupFolderFullPath error: nil])
			NSLog(@"[%s] Nem sikerült a backup mappát törölni: %@.", __FUNCTION__, backupFolderFullPath);
		else
			[valtoztatottJoguFajlok removeObjectForKey: backupFolderFullPath]; // eltavolitjuk, mert hibat fog dobni, ha nem tavolitjuk el
	}
	else {
		NSLog(@"[%s] Nem létezik a backup mappa: %@.", __FUNCTION__, backupFolderFullPath);
	}
	
	
	// visszaallitjuk a fajljogokat, mar ha van mit visszaallitani
	if ([valtoztatottJoguFajlok count] > 0) {
		// johet a jogok atallitasa
		NSMutableArray *arguments = [NSMutableArray new];
		
		[valtoztatottJoguFajlok enumerateKeysAndObjectsUsingBlock: ^(NSString *path, NSNumber *permission, BOOL *stop) {
			[arguments addObject: path];
			[arguments addObject: [NSString stringWithFormat: @"%o", [permission shortValue]]];
		}];
		
		NSString *error = nil;
		NSString *output = nil;
		if ([self runProcessAsAdministrator: [[NSBundle mainBundle] pathForResource: @"permissions" ofType: @"sh"]
							  withArguments: arguments
									 output: &output
						   errorDescription: &error]) {
			
			if (error)
				NSLog(@"%s {back run ok} AppleScript error: %@", __FUNCTION__, error);
			
			if (!output) {
				NSLog(@"%s {back run ok} Nincs output a scriptnél", __FUNCTION__);
			}
			else if(![output isEqualToString: @"ok"]) {
				NSLog(@"%s {back run ok} Hiba a shell script futtatásánál: %@", __FUNCTION__, output);
			}
		}
		else {
			NSLog(@"%s {back run fail} Nem futott le a shell script", __FUNCTION__);
			
			if (error)
				NSLog(@"%s {back run fail} AppleScript error: %@", __FUNCTION__, error);
			
			if (output) {
				NSLog(@"%s {back run fail} Output a scriptnél: %@", __FUNCTION__, output);
			}
		}
		
		[arguments release];
	}
	
	[valtoztatottJoguFajlok release];
	
	return !vanfail;
}
/*
#pragma mark - other functions
- (BOOL) isFilePath: (NSString *) path equalToMD5Hash: (NSString *) hash {
	NSData *data = [NSData dataWithContentsOfFile: path];
	if (!data)
		return NO;

	return [data isEqualToMD5Hash: hash];
}
*/

- (BOOL) runProcessAsAdministrator: (NSString *) scriptPath
					 withArguments: (NSArray *) arguments
							output: (NSString **) output
				  errorDescription: (NSString **) errorDescription; {
	
	// replace spaces, aposthrofs, and qoutes
	NSString *clearedScriptPath = [[[scriptPath stringByReplacingOccurrencesOfString: @"'" withString: @"\\\\'"]
									stringByReplacingOccurrencesOfString: @"\"" withString: @"\\\\\\\""] // the backslashes need the shell script and applescript. 2 x 2 need the applescript, 1 x 2 need the shell script and one is the escape character for xcode
								   stringByReplacingOccurrencesOfString: @" " withString: @"\\\\ "];
	
	NSMutableString * allArgs = [NSMutableString new];
	[arguments enumerateObjectsUsingBlock: ^(NSString *path, NSUInteger idx, BOOL *stop) {
		NSString *clearedPath = [[[path stringByReplacingOccurrencesOfString: @"'" withString: @"\\\\'"]
								  stringByReplacingOccurrencesOfString: @"\"" withString: @"\\\\\\\""] // the backslashes need the shell script and applescript. 2 x 2 need the applescript, 1 x 2 need the shell script and one is the escape character for xcode
								 stringByReplacingOccurrencesOfString: @" " withString: @"\\\\ "];
		[allArgs appendFormat: @"%@", clearedPath];
		if (idx < [arguments count] - 1)
			[allArgs appendString: @" "];
	}];
	
	NSString * fullScript = @"";
	if ([allArgs isEqualToString: @""])
		fullScript = clearedScriptPath;
	else
		fullScript = [NSString stringWithFormat: @"%@ %@", clearedScriptPath, allArgs];
	[allArgs release];
	
	NSDictionary *errorInfo = [NSDictionary new];
	NSString *script = [NSString stringWithFormat: @"do shell script \"%@\" with administrator privileges", fullScript];
	
	NSAppleScript *appleScript = [[NSAppleScript new] initWithSource: script];
	NSAppleEventDescriptor * eventResult = [appleScript executeAndReturnError: &errorInfo];
	[appleScript release];
	
	// Check errorInfo
	if (!eventResult) {
		// Describe common errors
		*errorDescription = nil;
		if ([errorInfo valueForKey: NSAppleScriptErrorNumber]) {
			NSNumber * errorNumber = (NSNumber *)[errorInfo valueForKey: NSAppleScriptErrorNumber];
			if ([errorNumber intValue] == -128)
				*errorDescription = @"The administrator password is required to do this.";
		}
		
		// Set error message from provided message
		if (*errorDescription == nil) {
			if ([errorInfo valueForKey: NSAppleScriptErrorMessage])
				*errorDescription =  (NSString *)[errorInfo valueForKey: NSAppleScriptErrorMessage];
		}
		
		return NO;
	}
	else {
		// Set output to the AppleScript's output
		*output = [eventResult stringValue];
        
		return YES;
	}
}

@end
